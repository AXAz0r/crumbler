FROM python:3.9-alpine

RUN mkdir -p /app
WORKDIR /app

COPY ./ ./

RUN apk add --no-cache build-base
RUN python -m pip install --no-cache-dir -U pip virtualenv \
    && python -m venv .venv \
    && source .venv/bin/activate \
    && python -m pip install -U pip \
    && pip install -Ur requirements.txt

ENTRYPOINT ["/bin/sh"]
CMD ["./run.sh"]

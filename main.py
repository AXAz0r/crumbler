from core.kuki import KukiCore


def main():
    core = KukiCore()
    core.start()
    # core.test()


if __name__ == '__main__':
    main()

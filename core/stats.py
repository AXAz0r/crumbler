import abc
import secrets

import arrow

from core.config import KukiConfig
from core.database import KukiDatabase


class StatItem(abc.ABC):
    def __init__(self, cfg: KukiConfig):
        super().__init__()
        self.count = 0
        self.pool = []
        while len(self.pool) < cfg.timeout.chance:
            num = secrets.randbelow(100)
            if num not in self.pool:
                self.pool.append(num)
        self.pool.sort()
        self.timestamp = None
        self.number = None
        self.message = None

    @staticmethod
    def roll() -> int:
        return secrets.randbelow(100)

    def trigger(self, num: int) -> bool:
        self.count += 1
        return num in self.pool

    def seal(self, num: int, msg: str):
        if not self.timestamp and not self.message and not self.number:
            self.timestamp = arrow.utcnow().int_timestamp
            self.number = num
            self.message = msg

    def to_dict(self) -> dict:
        return {
            'count': self.count,
            'pool': self.pool,
            'number': self.number,
            'timestamp': self.timestamp,
            'message': self.message
        }


class KukiStatistics(abc.ABC):
    def __init__(self, db: KukiDatabase):
        self.db = db
        self.stats = []
        self.load_stats()

    def load_stats(self):
        stats = self.db.get('stats')
        if stats is None:
            stats = []
        self.stats = stats

    def add_stat(self, stat: StatItem):
        self.stats.append(stat.to_dict())
        self.db.set('stats', self.stats)

import logging
import os

import arrow


def create_logger(name, *, to_title=False, level=None):
    if to_title:
        logname = titleize(name)
    else:
        logname = name
    return Logger.create(logname, level=level)


def titleize(string):
    new_string = ""
    for i, char in enumerate(string):
        if char.isupper() and i != 0:
            new_string += " " + char
        else:
            new_string += char
    return new_string


class Logger(object):
    __slots__ = (
        "default_fmt", "default_date_fmt", "name",
        "created", "_logger"
    )

    loggers = {}

    def __init__(self, name, *, level=None):
        self.default_fmt = '[ {levelname:^8s} | {asctime:s} | {name:<4.4s} ] {message:s}'
        self.default_date_fmt = '%Y.%m.%d %H:%M:%S'
        self.name = name
        self._logger = logging.getLogger(self.name)
        self._logger.setLevel(level or logging.DEBUG)
        self.created = False

    @classmethod
    def get(cls, name, *, level=None):
        if name not in cls.loggers.keys():
            cls.loggers.update({name: cls(name, level=level)})
        return cls.loggers.get(name)

    def info(self, message):
        self._logger.info(message)

    def debug(self, message):
        self._logger.debug(message)

    def error(self, message):
        self._logger.error(message)

    def warn(self, message):
        self.warning(message)

    def warning(self, message):
        self._logger.warning(message)

    def exception(self, message):
        self._logger.exception(message)

    # def create(cls, name, *, level=None, shards=None):
    @classmethod
    def create(cls, name, *, level=None):
        logger = cls.get(name, level=level)
        if logger.created:
            return logger
        cls.add_stdout_handler(logger)
        # cls.add_file_handler(logger, shards)
        logger.created = True
        return logger

    def add_handler(self, handler, fmt=None, date_fmt=None):
        fmt = fmt or self.default_fmt
        date_fmt = date_fmt or self.default_date_fmt
        handler.setFormatter(logging.Formatter(fmt=fmt, datefmt=date_fmt, style='{'))
        self._logger.addHandler(handler)

    @staticmethod
    def add_stdout_handler(logger):
        """
        Add a log handler that logs to the standard output.
        :type logger: ayu.core.mechanics.logger.Logger
        """
        handler = logging.StreamHandler()
        logger.add_handler(handler)

    @staticmethod
    def add_file_handler(logger, shards=None):
        log_dir = 'logs'
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)
        now = arrow.utcnow()
        if shards is not None:
            format_name = f'kuki.{"-".join([str(shard) for shard in shards])}.{now.format("YYYY-MM-DD")}.log'
        else:
            format_name = f'kuki.{now.format("YYYY-MM-DD")}.log'
        filename = os.path.join(log_dir, format_name)
        handler = logging.FileHandler(filename, encoding='utf-8')
        logger.add_handler(handler)

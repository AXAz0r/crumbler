import abc
import secrets

import requests
import twitch

from core.config import KukiConfig
from core.database import KukiDatabase
from core.logger import create_logger
from core.stats import KukiStatistics, StatItem


class KukiCore(abc.ABC):
    def __init__(self):
        super().__init__()
        self.cfg = KukiConfig()
        self.log = create_logger('KUKI')
        self.cfg.describe(self.log)
        self.db = KukiDatabase(self.log)
        self.helix = twitch.Helix(
            self.cfg.auth.helix.id,
            self.cfg.auth.client.secret,
            bearer_token=self.cfg.auth.helix.access
        )
        self.chat = twitch.Chat(
            f'#{self.cfg.users.broadcaster.name}',
            self.cfg.users.moderator.name,
            self.cfg.auth.client.oauth,
            self.helix
        )
        self.stat = StatItem(self.cfg)
        self.stats = KukiStatistics(self.db)
        self.karma = 0

    def handle(self, msg: twitch.chat.Message):
        if msg.sender.lower() == self.cfg.users.target.name.lower() or msg.user.id == self.cfg.users.target.id:
            rolls = 0
            number = None
            activated = False
            while rolls <= self.karma // 2 and not activated:
                number = self.stat.roll()
                activated = self.stat.trigger(number)
                rolls += 1
            pool = [str(pn) for pn in self.stat.pool]
            self.log.info(
                f'USR: {msg.sender} | NUM: {number} | POOL: {", ".join(pool)} | MSG: {msg.text}'
            )
            if activated:
                self.stat.seal(number, msg.text)
                self.stats.add_stat(self.stat)
                if self.timeout():
                    msg.chat.send(f'The Kuki crumbled by rolling a {number}...')
                else:
                    msg.chat.send(f'The Kuki would\'ve crumbled, but my code failed...')
                self.stat = StatItem(self.cfg)
                self.karma = 0
            else:
                self.karma += 1

    def timeout(self) -> bool:
        data = {
            'data': {
                'duration': self.cfg.timeout.duration,
                'reason': 'Automatic roulette ban.',
                'user_id': self.cfg.users.target.id
            }
        }
        headers = {
            'Authorization': f'Bearer {self.cfg.auth.helix.access}',
            'Client-ID': self.cfg.auth.helix.id,
            'Content-Type': 'application/json'
        }
        base = 'https://api.twitch.tv/helix/moderation/bans'
        uri = f'{base}?broadcaster_id={self.cfg.users.broadcaster.id}&moderator_id={self.cfg.users.moderator.id}'
        try:
            requests.post(uri, headers=headers, json=data)
            result = True
        except Exception as err:
            self.log.error(f'Timeout call errored: {err}!')
            result = False
        return result

    def start(self):
        self.log.info('Starting the Kuki monitor...')
        self.chat.subscribe(self.handle)

    def test(self):
        self.log.info('Running test...')
        for _ in range(100):
            stat = StatItem(self.cfg)
            offset = secrets.randbelow(3600 * 24 * 30)
            while stat.timestamp is None:
                num = stat.roll()
                act = stat.trigger(num)
                if act:
                    stat.seal(num, secrets.token_hex(32))
                    stat.timestamp -= offset
                    self.stats.add_stat(stat)
        self.log.info('Test finished!')

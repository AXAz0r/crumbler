import abc
import errno
import os
from typing import Optional

import yaml

from core.logger import Logger

CFG_PATH = './config/core.yml'


class KukiConfig(abc.ABC):
    def __init__(self):
        super().__init__()
        self.raw = {}
        self.load_config()
        self.auth = AuthConfig(self.raw.get('auth'))
        self.timeout = TimeoutConfig(self.raw.get('timeout'))
        self.users = UsersConfig(self.raw.get('users'))

    def load_config(self):
        if os.path.exists(CFG_PATH):
            with open(CFG_PATH, 'r', encoding='utf-8') as cfg_file:
                self.raw = yaml.safe_load(cfg_file.read())
        else:
            exit(errno.ENOENT)

    def describe(self, log: Logger):
        self.timeout.describe(log)
        self.users.describe(log)


class AuthConfig(abc.ABC):
    def __init__(self, raw: Optional[dict]):
        super().__init__()
        self.raw = raw if raw is not None else {}
        self.client = ClientConfig(self.raw.get('client'))
        self.helix = HelixConfig(self.raw.get('helix'))


class ClientConfig(abc.ABC):
    def __init__(self, raw: Optional[dict]):
        super().__init__()
        self.raw = raw if raw is not None else {}
        self.id = self.raw.get('id')
        self.secret = self.raw.get('secret')
        self.oauth = self.raw.get('oauth')


class HelixConfig(abc.ABC):
    def __init__(self, raw: Optional[dict]):
        super().__init__()
        self.raw = raw if raw is not None else {}
        self.id = self.raw.get('id')
        self.access = self.raw.get('access')
        self.refresh = self.raw.get('refresh')


class TimeoutConfig(abc.ABC):
    def __init__(self, raw: Optional[dict]):
        super().__init__()
        self.raw = raw if raw is not None else {}
        self.chance = self.raw.get('chance')
        self.duration = self.raw.get('duration')

    def describe(self, log: Logger):
        log.info(f'Chance: {self.chance}%')
        log.info(f'Duration: {self.duration} sec')


class UsersConfig(abc.ABC):
    def __init__(self, raw: Optional[dict]):
        super().__init__()
        self.raw = raw if raw is not None else {}
        self.target = UserConfig(self.raw.get('target'))
        self.moderator = UserConfig(self.raw.get('moderator'))
        self.broadcaster = UserConfig(self.raw.get('broadcaster'))

    def describe(self, log: Logger):
        self.target.describe(log, 'target')
        self.moderator.describe(log, 'moderator')
        self.broadcaster.describe(log, 'broadcaster')


class UserConfig(abc.ABC):
    def __init__(self, raw: Optional[dict]):
        super().__init__()
        self.raw = raw if raw is not None else {}
        self.id = self.raw.get('id')
        self.name = self.raw.get('name')

    def describe(self, log: Logger, name: str):
        log.info(f'{name.title()} ID: {self.id}')
        log.info(f'{name.title()} Name: {self.name}')

import abc
import json
import os
from typing import Union

from core.logger import Logger

DB_PATH = './data/database.json'


class KukiDatabase(abc.ABC):
    def __init__(self, log: Logger):
        super().__init__()
        self.data = {}
        self.log = log
        self.load_data()

    def load_data(self):
        self.log.info('Loading database...')
        if os.path.exists(DB_PATH):
            with open(DB_PATH, 'r', encoding='utf-8') as db_file:
                self.data = json.loads(db_file.read())
        else:
            self.log.info('No database file found, starting with nothing.')

    def get(self, key: str) -> Union[int, str, bool, dict, list]:
        return self.data.get(key)

    def set(self, key: str, val: Union[int, str, bool, dict, list]):
        self.data.update({key: val})
        self.save_data()

    def save_data(self):
        with open(DB_PATH, 'w', encoding='utf-8') as db_file:
            db_file.write(json.dumps(self.data))
